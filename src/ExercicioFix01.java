import java.util.Scanner;


public class ExercicioFix01 {
	
	public static void main(String[] args) {
		
		/**
		 Exercício Fixação 01
		 a. Criar um programa para processar as seguintes 
		 informações de um aluno:matrícula, nome , nota1, 
		 nota2 e se ele está aprovado ou reprovado;
		 b. Durante a execução calcular a nota final do aluno 
		 utilizando a seguinte regra: notaFinal = (nota1 + nota2) / 2. 
		 Se a nota final for igual ou superior a 6 o aluno está aprovado, 
		 caso contrário estará reprovado;

		 O programa deverá imprimir o seguinte relatório:
		 
		 Matrícula: xxxxx
         Nome: xxxxx xxxxx
         Aprovado: ( x ) Sim ( ) Não
         Nota final: xxxxx
		 
		*/
		
		Scanner leitor = new Scanner(System.in);
		 
		String matricula;
		String nome;
		int nota1;
		int nota2;
		
		System.out.println("Digite a matrícula");
		matricula = leitor.nextLine();
		
		System.out.println("Digite o nome:");
		nome = leitor.nextLine();
		
		System.out.println("Digite a primeira nota:");
		nota1 = leitor.nextInt();
		
		System.out.println("Digite a segunda nota:");
		nota2 = leitor.nextInt();
		
		int notaFinal = (nota1 + nota2) /2;
		
		
		System.out.println("Matrícula: " + matricula);
		System.out.println("Nome: " + nome);
		                   
		if (notaFinal >= 6) {
			System.out.println("Aprovado: (x) Sim ( ) Não");
		} else {
			System.out.println("Aprovado: ( ) Sim (x) Não");
		}
		
		System.out.println("Nota final: " + notaFinal);
		
		
		leitor.close();
	}

}
